.. meta::
   :keywords: AWS, CyberSecurity, CloudSecurity, CloudOptics
   :description lang=en: Following guide prepares an AWS account to 
       be onboarded to CloudOptics Digital Security platform.


Prepare AWS Account for Onboarding (Cross Account)
==================================================

Please follow the instruction to prepare your AWS account for onboardiing into the product.


1. Sign-in to AWS Console & go to IAM Service to create a new role "CloudOpticsRole"
   Please follow the actions in the screenshot below. 

   Please note account number (673199402158) and external id (cloudoptics) needs to exactly match. 

.. figure:: /_static/x_account_create_role_1.png
   :align:   center

--------------------------------------------------------------------------------------

2. Please use following IAM Permissions to add to the role being created

   * ReadOnlyAccess
   * SecurityAudit
   * AWSCloudTrailReadOnlyAcccess
   * CloudWatchReadOnlyAccess

.. figure::  /_static/x_account_create_role_2.png
   :align:   center

--------------------------------------------------------------------------------------

3. Verify the name to create the role

.. figure::  /_static/x_account_create_role_3.png
   :align:   center

--------------------------------------------------------------------------------------

4. After creating the role, go the role and create inline policy

.. figure::  /_static/x_account_create_role_4.png
   :align:   center

--------------------------------------------------------------------------------------

5. Download cloudoptics_policy.json from `here`_.

.. _here: https://cloudoptics-public-policy.s3.ap-south-1.amazonaws.com/cloudoptics_policy.json

--------------------------------------------------------------------------------------

6. Add cloudoptics_policy.json as per the image

.. figure::  /_static/x_account_create_role_5.png
   :align:   center

--------------------------------------------------------------------------------------

7. Make a note of role ARN. It will be needed to onboard account into CloudOptics.

--------------------------------------------------------------------------------------

Please follow further instructions for CNAPP

8. Download co_kms_key_policy.json from `link`_.

.. _link: https://cloudoptics-public-policy.s3.ap-south-1.amazonaws.com/co_kms_key_policy.json

--------------------------------------------------------------------------------------

9. Create a KMS key in the region of your workloads as per the image

	* KMS Key Alias				: CloudOptics-KMS-Key
	* Key Administrator			: CloudOpticsRole
	* AWS Account to be added	: 673199402158

.. figure::  /_static/cwpp_key_5.png
   :align:   center

--------------------------------------------------------------------------------------

10. Edit the KMS Key policy as per the image and insert co_kms_key_policy.json contents here

.. figure::  /_static/cwpp_key_6.png
   :align:   center


