Infra Vulnerability Assessment (AWS Only)
=========================================

Using CloudOptics, you could scan each of your AWS virtual machines and create an actionable report. 
Users of cloudOptics need to order suitable assessment to use this service.

This service is available only for AWS account right now.

This is one time activity. Follow these steps to enable your AWS account for onboarding into this service. These steps need to be repeated for each of the regions in use.

1. Open the "Systems Manager" service and go to quick setup.

.. figure::  /_static/ssm_1.png
   :align:   center

2. Choose the options as suggested in the guide below.

.. WARNING:: We recommend using tags to select assets, however if VMs are not tagged correctly then manual addition may be required. 

.. figure::  /_static/ssm_2.png
   :align:   center

3. Go to CloudOpticsGroup and add following permission to the group

* AmazonInspectorFullAccess

Post addition group should like below.

.. figure::  /_static/ssm_3.png
   :align:   center

After enabling AWS account go to Advisory Assessment panel in CloudOptics and order the scan.

