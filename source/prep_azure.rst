Prepare Azure Account for Onboarding
====================================

Please follow the instruction to prepare your Azure account for onboardiing into the product.

1. Sign-in to https://portal.azure.com/ Console & click as directed in screen below

.. figure:: /_static/Azure_app_reg1.png
   :align:   center

--------------------------------------------------------------------------------------

2. Register a new application, with following information

   * Display Name - CloudOptics
   * Home Page - Intended login URL for CloudOptics

Once created, copy Application Id value in a notepad as "Client Id"

.. figure::  /_static/Azure_app_reg2.png
   :align:   center

--------------------------------------------------------------------------------------

3. Click on "Settings" then further on "Keys" as per screen below

.. figure::  /_static/Azure_app_reg3.png
   :align:   center

--------------------------------------------------------------------------------------

4. Create a new key with name "CloudOptics Key", expiry date as "Never Expires" and hit save.
Once saved value filed will be shown. Please copy the value field in a notepad as "Azure Secret Key"

.. WARNING:: This value will not be shown again. So it is important to make a note of it.

.. figure::  /_static/Azure_app_reg4.png
   :align:   center

--------------------------------------------------------------------------------------

5. Go back to portal home and follow the sequence as directed below and copy the Directory ID as "Tenant Id"

.. figure::  /_static/Azure_app_reg5.png
   :align:   center

--------------------------------------------------------------------------------------

6. From the portal, find out "Subscription ID"

.. figure::  /_static/Azure_app_reg6.png
   :align:   center

--------------------------------------------------------------------------------------

7. You should now have 4 values in the notes. These values will be used in CloudOptics to onboard this Azure account

	* Client ID
	* Secret Key
	* Tenant Id
	* Subscription Id
   
--------------------------------------------------------------------------------------

8. Go to relevant Azure subscription and open Access Control (IAM) and click "Add"

.. figure::  /_static/Azure_subs2.png
   :align:   center

   
--------------------------------------------------------------------------------------

9. Add the permissions of a "Reader" role to "CloudOptics" Application

.. figure::  /_static/Azure_subs1.png
   :align:   center

Your Azure Account is now ready to be added in CloudOptics

