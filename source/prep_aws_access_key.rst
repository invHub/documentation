.. meta::
   :keywords: AWS, CyberSecurity, CloudSecurity, CloudOptics
   :description lang=en: Following guide prepares an AWS account to 
       be onboarded to CloudOptics Digital Security platform.


Prepare AWS Account for Onboarding (Access Key)
===============================================

Please follow the instruction to prepare your AWS account for onboardiing into the product.
This uses AccessKey method. Please note, CWPP features will not be accessible usingthis approach.

1. Sign-in to AWS Console & go to IAM Service to create a new group "CloudOpticsGroup"

.. figure:: /_static/iam_group_1.png
   :align:   center

--------------------------------------------------------------------------------------

2. Please use following IAM Permissions to add to group


   * ReadOnlyAccess
   * AWSCloudTrailReadOnlyAcccess
   * CloudWatchReadOnlyAccess

.. figure::  /_static/iam_group_2.png
   :align:   center

--------------------------------------------------------------------------------------

3. Verify the name & permissions in next screen to create the group

.. figure::  /_static/iam_group_3.png
   :align:   center

--------------------------------------------------------------------------------------

4. Go to AddUser in AWS IAM Console

.. figure::  /_static/iam_user_1.png
   :align:   center

--------------------------------------------------------------------------------------

5. Add user CloudOptics with ProgrammaticAccess

.. figure::  /_static/iam_user_2.png
   :align:   center

--------------------------------------------------------------------------------------

6. Next we will add this user to "CloudOpticsGroup"

.. figure::  /_static/iam_user_3.png
   :align:   center

--------------------------------------------------------------------------------------

7. Copy Access Key ID & Secrete Access Key in a notepad as shown in screen below. We will need it to onboard AWS account into CloudOptics

.. WARNING:: The Secret Key will not be shown again. So it is important to make a note of it.
   
   
.. figure::  /_static/iam_user_4.png
   :align:   center


