Advisory Assessment 
====================

Using CloudOptics, you could do one time assessments for your cloud accounnt.
Various assement options such as Security, Cost, Compliance assessments are available.

At high level following steps need to be followed -

1. Place an order
2. Add a cloud account to order
3. Download Sample Report (Optional)
4. Pay for the assessment
5. Download Report(s)


Placing An Assessment Order
---------------------------

1. Open the order dialog box by clicking on + icon in "Advisory Assessment" product selection

.. figure::  /_static/aa_1.png
   :align:   center

2. Complete the order wizard by entering estimated VMs and selecting assessment options

.. figure::  /_static/aa_2.png
   :align:   center

3. You should receive an email indicating successful order placement within 10 minutes.

Adding Cloud Account To Order
-----------------------------

1. On the newly placed order, click on + icon to add cloud account. Account preparation
instructions link is there in the popup.

.. figure::  /_static/aa_3.png
   :align:   center

2. As soon as account is added, assessment begins and an email  notification is issued 
indicating successful addition to assessment order.

Download Sample Report
----------------------

On completion of assessment, an email notification is sent. Most accounts finish assessments within
30 minutes. It may take longer depending on number of resources discovered in your account.

Using following button, all sample reports may be downloaded. Sample reports contain only a subset of 
assessment results and PSD exports are watermarked with text "Sample".

.. figure::  /_static/aa_4.png
   :align:   center


Pay For Report
--------------

Completed order display line items and prices based on resources detected in the account.
All major credit cards are accepted for payment. We use Stripe payment system.

Download Report(s)
------------------

After payment order status changes to "Download Report" and all ordered assessment reports can be 
reviewed/download by clicking report icon.

.. figure::  /_static/aa_5.png
   :align:   center


