CloudOptics Documentation
==========================

Please follow the instructions to install & configure the product.

.. toctree::
   :maxdepth: 2

   introduction

.. toctree::
   :maxdepth: 2

   intial_config

.. toctree::
   :maxdepth: 2

   onboard_cloud_accounts

.. toctree::
   :maxdepth: 2

   add_accounts_to_cloudoptics

.. toctree::
   :maxdepth: 2

   advisory_assessment

.. toctree::
   :maxdepth: 2

   threat_intel

.. toctree::
   :maxdepth: 2

   infra_va

