Prepare & Onboard Cloud Accounts
================================

CloudOptics supports various types of cloud integrations. Before onboarding cloud accounts need to be prepared for CloudOptics.
Please use specific guide for integrating your cloud.

.. toctree::
   :maxdepth: 1

   prep_aws_access_key
   prep_aws_cross_account
   prep_aws_billing
   prep_azure
   prep_gcp
   prep_openstack
   
