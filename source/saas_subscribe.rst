Subscribing CloudOptics as SaaS
================================

CloudOptics is available as SaaS for organiztions to subscribe online and consume.
Please follow instructionsd below to use SaaS -

1. Please visit https://app.cloudoptics.io/#/setup & register

.. figure:: /_static/saas_register1.png
   :align:   center

2. Please follow on screen instructions to complete the registration


