.. meta::
   :keywords: AWS, CyberSecurity, CloudSecurity, CloudOptics
   :description lang=en: Following guide prepares an AWS account to 
       be onboarded to CloudOptics Digital Security platform for chargeback


Prepare AWS Account Billing for Onboarding
==========================================

Please follow the instruction to prepare your AWS account billing for onboardiing into the product.


1. Sign-in to AWS Console & create a S3 bucket to export billing data

--------------------------------------------------------------------------------------

2. Please navigate to AWS Billing dashboard and click on create report

.. figure::  /_static/aws_billing_1.png
   :align:   center

--------------------------------------------------------------------------------------

3. Provide the bill report name & select Resource Id

.. figure::  /_static/aws_billing_2.png
   :align:   center

--------------------------------------------------------------------------------------

4. Configure S3 target bucket for report delivery and select rest of the options as shown
below.

.. figure::  /_static/aws_billing_3.png
   :align:   center

--------------------------------------------------------------------------------------

5. Go to IAM section and create a policy named "CostExplorerAPI"

.. code-block:: bash

		{
			"Version": "2012-10-17",
			"Statement": [
				{
					"Effect": "Allow",
					"Action": [
						"ce:*",
						"cur:DescribeReportDefinitions"
					],
					"Resource": [
						"*"
					]
				}
			]
		}

--------------------------------------------------------------------------------------

6. Go to "CloudOptics" user and attach "CostExplorerAPI" policy to the user.

.. figure::  /_static/aws_billing_4.png
   :align:   center

--------------------------------------------------------------------------------------

