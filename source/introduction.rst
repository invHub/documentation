Introduction
=============

CloudOptics has the ability to manage a single cloud account or multiple clouds.
Product also has the capaility to onboard multiple customers at the same time, 
having various cloud accounts of their own.

Following guide will help choose the right product edition to get started.

Trial version of the product comes with a no obligation 2-weeks entitlement.
Trial version comes fully fnctional with all modules of CloudOptics to secure 
your critiical infrastructure.

Please select the target environment where you want to install the product for
detailed steps.

.. toctree::
   :maxdepth: 1

   saas_subscribe
   install_on_privatecloud
   install_on_awscloud

