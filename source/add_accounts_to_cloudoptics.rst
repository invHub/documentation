Add Cloud Accounts to CloudOptics
=================================

Before onboarding, cloud accounts need to be prepared for CloudOptics.
If you have not prepared yor AWS/Azure accounts yet, please come back after making those changes.

After preparing the target cloud accounts, login to https://app.cloudoptics.io/#/login as Administrator

Adding an AWS Account
---------------------

1. Click on + "Create Account" under "Security Monitoring", select "AWS" from account type and provide requested information 

.. figure::  /_static/co_aws_create_account.png
   :align:   center

Adding an Azure Account
-----------------------

1. Click on + "Create Account" under "Security Monitoring", select "Azure" from account type and provide requested information

.. figure::  /_static/co_azure_create_account.png
   :align:   center

Adding Google Cloud Account
----------------------------

1. Click on + "Create Account" under "Security Monitoring", select "Azure" from account type and provide requested information

.. figure::  /_static/co_gcp_create_account.png
   :align:   center

