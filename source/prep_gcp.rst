Prepare Google Cloud Account for Onboarding
===========================================

Please follow the instruction to prepare your Google Cloud Account for onboardiing into the product.

1. Sign-in to https://console.cloud.google.com Console & select the project, you want rto onboard 


2. Start with creating a custom 'Viewer' role for CloudOptics. This role will be created from Google default role 'Viewer'

.. figure::  /_static/gcp_create_role_1.png
   :align:   center


3. Search and add following permissions for the role

	* storage.buckets.get
	* storage.buckets.getIamPolicy
	* storage.buckets.list
	* storage.objects.getIamPolicy
	* storage.objects.list

   Verify the permission as per screen below.
   
.. figure::  /_static/gcp_create_role_2.png
   :align:   center

--------------------------------------------------------------------------------------

4. Create a Service Account for the project

.. figure::  /_static/gcp_create_acc_1.png
   :align:   center

--------------------------------------------------------------------------------------

5. Add custom role created in step #2 above to the service account

.. figure::  /_static/gcp_create_acc_2.png
   :align:   center

--------------------------------------------------------------------------------------

6. Create a JSON key for the service account and save it on your local computer.

.. figure::  /_static/gcp_create_acc_3.png
   :align:   center

.. figure::  /_static/gcp_create_acc_4.png
   :align:   center

.. WARNING:: This JSON will not be shown again. So it is important to save it.


--------------------------------------------------------------------------------------

7. Navigate to API & Access area of the dashboard for the project

.. figure::  /_static/gcp_api_enable_1.png
   :align:   center

.. figure::  /_static/gcp_api_enable_2.png
   :align:   center

--------------------------------------------------------------------------------------

8. Enable Compute API & verify access as per screenshot below
	
.. figure::  /_static/gcp_compute_api_enable_verif.png
   :align:   center

--------------------------------------------------------------------------------------

9. Enable IAM API & verify access as per screenshot below

.. figure::  /_static/gcp_iam_enable_api_verif.png
   :align:   center

--------------------------------------------------------------------------------------

10. Enable KMS API & verify access as per screenshot below
  
.. figure::  /_static/gcp_kms_enable_api_verif.png
   :align:   center

--------------------------------------------------------------------------------------

11. Enable Resource Manager API & verify access as per screenshot below
   
.. figure::  /_static/gcp_resmgr_enable_verify.png
   :align:   center

--------------------------------------------------------------------------------------

12. Enable Storage API & verify access as per screenshot below
   
.. figure::  /_static/gcp_storage_enable_api_verif.png
   :align:   center

    
Your Google Cloud Account is now ready to be added in CloudOptics

