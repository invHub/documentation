Install CloudOptics on a Server
================================

In order to install CloudOptics on any machine of your choice in any environment,
please follow these instructions.

Elibility
----------

1. CloudOptics platform license can be procured by a Managed Service Provider (MSP)
by writing to sales@cloudoptics.io Post email, download authorization will be provided 
along with a license key.

MSP version allows a company to host CloudOptics platform in a multi-tenant mode
so all of their customer can be serviced directly by the MSP

2. CloudOptics platform license can also be procured by an end customer for its own use,
by writing to sales@cloudoptics.io Post email, download authorization will be provided 
along with a license key.

End user version allows a company to host CloudOptics platform in a their account
in a single tenant mode, so all of their cloud accounts can be onboarded and monitored.


Pre-Requisite
--------------

We recommend following machine configuration for installing CloudOptics

* Ubuntu 18.04 Operating System
* sudo access on the machine
* 16GB RAM, 4 vCPU, 100GB Hard Disk 
* `Docker Community Edition <https://store.docker.com/search?type=edition&offering=community>`_

Networking Requirement
----------------------

* Inbound access on port 8080 to access Platform console
* Outbound access to connect with various target clouds & Licensing Server


Quick Installation
-------------------

Please execute following command to get the required script.

.. code-block:: bash

   curl -sO http://remote.cloudoptics.io/install.sh

   chmod +x install.sh


Execute following command to start the installation

.. code-block:: bash

   ./install.sh

Installation script will configure public IP of the machine for accessing product console.


