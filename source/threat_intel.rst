Threat Intel Contextualization (AWS Only)
=========================================

Using CloudOptics, you could be aware of virtual machines affected by latest vulnerabilities 
as they become known. Users of cloudOptics need to subscribe to threat intel feed and hunt for
machines where they might be present. CloudOptics does it for you, automatically.

This service is available only for AWS account right now.

Follow these steps to enable your AWS account for onboarding into this service.

Configure Systems Manager
-------------------------

Please follow these steps for each of the regions in use.

1. Open the "Systems Manager" service and go to quick setup.

.. figure::  /_static/ssm_1.png
   :align:   center

2. Choose the options as suggested in the guide below.

.. WARNING:: We recommend using tags to select assets, however if VMs are not tagged correctly then manual addition may be required. 

.. figure::  /_static/ssm_2.png
   :align:   center

After enabling AWS account add this service to your account from subscription panel in CloudOptics. 

