Initial Configuration Of CloudOptics
====================================

Before you begin, please collect SMTP server details from you administator.

In your browser, please open url http://<public ip>:8080/#/setup

1. You should see following screen. 

.. figure::  /_static/lic_request.png
   :align:   center

Please fill the information correctly as the license generated will be against the entity.
This information may not be edited afterwards.

2. Successful license generation will present following screen.

.. figure::  /_static/admin_account.png
   :align:   center

3. Next you need to configure SMTP server details to receive emails

.. figure::  /_static/smtp_server.png
   :align:   center

Thats it!!! You should be greeted with the login page.

.. figure::  /_static/login_page.png
   :align:   center


